package conexao;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
	private static String URL = "jdbc:mysql://localhost:3306/sis_academico";
	private static String USER= "root";
	private static String PWD= "";
	
	static Connection con = null;
	
	public static Connection conectaBanco() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(URL, USER, PWD);
			
		}catch (ClassNotFoundException | SQLException e) {
			throw new RuntimeException("Erro", e);
			
		}return con;
		
	}	
	
	}
