package academico.DAO;

import java.util.List;

import academico.VO.Disciplina;

public interface DisciplinaDAO {
	
	List<Disciplina> listaDisciplinas();
	Disciplina findById(int cod_disciplina);
	Disciplina findByNome(String nome_disciplina);
	void cadastra(int cod_disciplina, String nome_disciplina, int matricula_prof);
}
