package academico.DAO;

import java.util.List;

import academico.VO.Curso;

public interface CursoDAO {
	
	List<Curso> listaCursos();
	Curso findByCod(int idCurso);
	Curso findByNome(String nomeCurso);
	void cadastra(int idCurso, String nomeCurso);
}
