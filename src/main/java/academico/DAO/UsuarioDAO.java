package academico.DAO;

import java.util.List;

import academico.VO.Usuario;

public interface UsuarioDAO {
	
	List<Usuario> listaUsuariosAlunos();
	List<Usuario> listaUsuariosProf();
	Usuario findById(int cod_user);
	void cadastraUser(int cod_user, String tipo_user, String psw_user);
	void alterPwd(String psw_user);
}
