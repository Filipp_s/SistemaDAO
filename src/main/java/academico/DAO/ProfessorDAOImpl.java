package academico.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import academico.VO.Professor;
import conexao.Conexao;

public abstract class ProfessorDAOImpl implements ProfessorDAO {

	@Override
	public List<Professor> listaProfessores() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Professor findById(int matricula_prof) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Professor findByNome(String nome_prof) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cadastra(Professor professor) {
		Connection conec = Conexao.conectaBanco();
		PreparedStatement pdst = null;
		
		try {
			pdst = conec.prepareStatement
				 ("INSERT INTO professor (matricula_prof,  nome_prof, disciplinas, cod_curso) VALUES (?, ?, ?, ?)");
				pdst.setInt(1, professor.getMatricula_prof());
				pdst.setString(2, professor.getNome_prof());
				pdst.setString(3, professor.getDisciplina());
				pdst.setInt(4, professor.getCod_curso());
				
				pdst.executeUpdate();
			
			}catch (SQLException e) {
				System.out.println(e);
			}

	}

	@Override
	public void cadastraDisciplina(String disciplina) {
		// TODO Auto-generated method stub

	}

	
}
