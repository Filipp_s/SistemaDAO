package academico.DAO;

import java.util.List;

import academico.VO.Professor;

public interface ProfessorDAO {
	
	List<Professor> listaProfessores();
	Professor findById(int matricula_prof);
	Professor findByNome(String nome_prof);
	void cadastraDisciplina(String disciplina);
	public void cadastra(int matricula_prof, String nome_prof, String disciplina, int cod_curso);
	public void cadastra(Professor professor);
}
