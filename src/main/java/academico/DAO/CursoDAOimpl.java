package academico.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import academico.VO.Curso;
import conexao.Conexao;

public class CursoDAOimpl {
	
	public List<Curso> findById(Curso cod_curso) {
		Connection conec = Conexao.conectaBanco();
		PreparedStatement pdst = null;
		ResultSet rs = null;
		
		ArrayList<Curso> idCurso = new ArrayList<>();
		
		try {
			pdst = conec.prepareStatement
					("select * from curso where cod_curso = ?");
					 rs = pdst.executeQuery();
					
					 while (rs.next()) {
						 Curso id = new Curso(); 
						 id.setCod_curso(rs.getInt("Cod_curso"));
						 idCurso.add(id);		
					 }
					}catch(SQLException e) {
						System.out.println(e );
					}		
		return idCurso;
	}
	
	public List<Curso> findByNome(Curso nome) {
		Connection conec = Conexao.conectaBanco();
		PreparedStatement pdst = null;
		ResultSet rs = null;
		
		ArrayList<Curso> nomeCurso = new ArrayList<>();
		
		try {
			pdst = conec.prepareStatement
					("select * from aluno where nome = ?");
					 rs = pdst.executeQuery();
					
					 while (rs.next()) {
						 Curso curso = new Curso(); 
						 curso.setNome_curso(rs.getString("nome_curso"));
						 nomeCurso.add(curso);		
					 }
					}catch(SQLException e) {
						System.out.println(e );
					}		
		return nomeCurso;
	}
	
	public void cadastra(Curso curso) {
		Connection conec = Conexao.conectaBanco();
		PreparedStatement pdst = null;
		
		try {
			pdst = conec.prepareStatement
				 ("INSERT INTO Curso (cod_curso, nome) VALUES (?, ?)");
				pdst.setInt(1, curso.getCod_curso());
				pdst.setString(2, curso.getNome_curso());
								
				pdst.executeUpdate();
				
			}catch (SQLException e) {
				System.out.println(e);
			}
		}
		
		public List<Curso> listaCursos(){
			PreparedStatement pdst = null;
			Connection conec = Conexao.conectaBanco();
			ResultSet rs = null;
			
			List<Curso> cursos = new ArrayList<>();
			
			try {
			 pdst = conec.prepareStatement("SELECT * FROM curso ORDER BY cod_curso");
			 rs = pdst.executeQuery();
			 
			 while (rs.next()) {
				 Curso curso = new Curso();
				 
				 curso.setCod_curso(rs.getInt("cod_curso"));
				 curso.setNome_curso(rs.getString("nome"));
				
					int idCurso2	 = rs.getInt(1);
					String nomeCurso = rs.getString(2);
					System.out.println("idCurso: " + idCurso2);
					System.out.println("nomeCurso: " + nomeCurso);
					System.out.println("");
					
				 cursos.add(curso);
				 			
			 }
			}catch(SQLException e) {
				System.out.println(e );
			}
			return cursos;
}
}
