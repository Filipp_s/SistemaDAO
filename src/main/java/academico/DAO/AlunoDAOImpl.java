package academico.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import academico.VO.Aluno;
import conexao.Conexao;

public abstract class AlunoDAOImpl implements AlunoDAO {
	
		
	public List<Aluno> findById(Aluno matricula_aluno) {
		Connection conec = Conexao.conectaBanco();
		PreparedStatement pdst = null;
		ResultSet rs = null;
		
		ArrayList<Aluno> idAluno = new ArrayList<>();
		
		try {
			pdst = conec.prepareStatement
					("select * from aluno where matricula_aluno = ?");
					 rs = pdst.executeQuery();
					
					 while (rs.next()) {
						 Aluno id = new Aluno(); 
						 id.setMatricula_aluno(rs.getInt("matricula_aluno"));
						 idAluno.add(id);		
					 }
					}catch(SQLException e) {
						System.out.println(e );
					}		
		return idAluno;
	}
	
	public List<Aluno> findByNome(Aluno nome_aluno) {
		Connection conec = Conexao.conectaBanco();
		PreparedStatement pdst = null;
		ResultSet rs = null;
		
		ArrayList<Aluno> nomeAluno = new ArrayList<>();
		
		try {
			pdst = conec.prepareStatement
					("select * from aluno where nome_aluno = ?");
					 rs = pdst.executeQuery();
					
					 while (rs.next()) {
						 Aluno nome = new Aluno(); 
						 nome.setNome_aluno(rs.getString("nome_aluno"));
						 nomeAluno.add(nome);		
					 }
					}catch(SQLException e) {
						System.out.println(e );
					}		
		return nomeAluno;
	}
		
		public void cadastra(Aluno a) {
	Connection conec = Conexao.conectaBanco();
	PreparedStatement pdst = null;
	
	try {

		@SuppressWarnings("resource")
		Scanner id = new Scanner(System.in);
		Scanner nome = new Scanner(System.in);
		Scanner turno = new Scanner(System.in);
		 
		System.out.println("Digite uo ID: ");
		int idAl = id.nextInt();
		 
		System.out.println("Digite nome: ");
		String nomeAl = nome.nextLine();
		 
		System.out.println("Digite o turno: ");
		String turnoAl = turno.nextLine();
		
		String query=
				 "INSERT INTO aluno (matricula_aluno, nome_aluno, turno" +
		         "VALUES(?, ?, ?)";
		 PreparedStatement p = conec.prepareStatement(query);
		 p.setInt(1, idAl);
		 p.setString(2, nomeAl);
		 p.setString(3, turnoAl);
		 
		 p.executeUpdate();
			System.out.println("ALUNO CADASTRADO COM SUCESSO!");
		}catch (SQLException e) {
			System.out.println(e);
		}
	}
	
	public List<Aluno> listaAlunos(){
		PreparedStatement pdst = null;
		Connection conec = Conexao.conectaBanco();
		ResultSet rs = null;
		
		List<Aluno> alunos = new ArrayList<>();
		
		try {
		 pdst = conec.prepareStatement("SELECT * FROM Aluno ORDER BY matricula_aluno");
		 rs = pdst.executeQuery();
		 
		 while (rs.next()) {
			 Aluno a = new Aluno();
			 
			 a.setMatricula_aluno(rs.getInt("matricula_aluno"));
			 a.setNome_aluno(rs.getString("nome_aluno"));
			 a.setTurno(rs.getString("turno"));
			 a.setCod_curso(rs.getInt("cod_curso"));
			 
			 	int matricula_aluno	 = rs.getInt(1);
				String nome_aluno = rs.getString(2);
				String turno = rs.getString(3);
				int codCurso = rs.getInt(4);
				System.out.println("matricula_aluno: " + matricula_aluno);
				System.out.println("nome_aluno: " + nome_aluno);
				System.out.println("turno: " + turno);
				System.out.println("codCurso: " + codCurso);
				System.out.println("");
			 
			 alunos.add(a);
			 			
		 }
		}catch(SQLException e) {
			System.out.println(e );
		}
		return alunos;
	}
	
	public void viewBoletim() {
		Connection con = Conexao.conectaBanco();
		try {
			 @SuppressWarnings("resource")
				Scanner ler = new Scanner(System.in);
				 
			 System.out.println("Digite um ID: ");
			 int IdAluno = ler.nextInt();
			 String query=
					 "select a.matricula_aluno, a.nome_aluno, d.nome_disciplina, valor_nota from sis_academico.nota n " + 
					 "inner join sis_academico.aluno a on a.matricula_aluno = n.matricula_aluno " + 
					 "inner join sis_academico.disciplina d on d.cod_disciplina = n.cod_disciplina " + 
					 "where a.matricula_aluno = ?";
			 PreparedStatement p = con.prepareStatement(query);
			 p.setInt(1, IdAluno);
			 ResultSet rs= p.executeQuery();

			 while (rs.next()){
					
					int matricula_aluno	 = rs.getInt(1);
					String nome_aluno = rs.getString(2);
					String nome_disciplina = rs.getString(3);
					int nota = rs.getInt(4);
					System.out.println("===================================================================");
					System.out.println("| IdAluno |     nome_aluno    |         Disciplina        |  nota |");
					System.out.println("-------------------------------------------------------------------");
					System.out.print("|     " +matricula_aluno);
					System.out.print("   |    " + nome_aluno);
					System.out.print("    |    " + nome_disciplina);
					System.out.println("    |  " + nota + "  |");
					System.out.println("-------------------------------------------------------------------");
					System.out.println("");
					}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	public void buscaByNome(String lido) {
		 Connection con=Conexao.conectaBanco();
		 
	 try {
		 @SuppressWarnings("resource")
			Scanner ler = new Scanner(System.in);
			 
		 System.out.println("Digite um nome: ");
		 String nome_Aluno = ler.nextLine();
		 String query="select * from aluno where nome_aluno=?";
		 PreparedStatement p=con.prepareStatement(query);
		 p.setString(1, nome_Aluno);
		 ResultSet rs= p.executeQuery();

		 while (rs.next()){
				
				int matricula_aluno	 = rs.getInt(1);
				String nome_aluno = rs.getString(2);
				String turno = rs.getString(3);
				int codCurso = rs.getInt(4);
				
				System.out.println("matricula_aluno: " + matricula_aluno);
				System.out.println("nome_aluno: " + nome_aluno);
				System.out.println("turno: " + turno);
				System.out.println("codCurso: " + codCurso);
				System.out.println("");
				}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	
}
	
	
		
}	
	


