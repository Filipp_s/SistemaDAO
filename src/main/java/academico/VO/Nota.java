package academico.VO;

public class Nota {
	double valor_nota;
	Disciplina cod_disciplina;
	Aluno matricula_aluno;
	
	public double getValor_nota() {
		return valor_nota;
	}
	public void setValor_nota(double valor_nota) {
		this.valor_nota = valor_nota;
	}
	public Disciplina getCod_disciplina() {
		return cod_disciplina;
	}
	public void setCod_disciplina(Disciplina cod_disciplina) {
		this.cod_disciplina = cod_disciplina;
	}
	public Aluno getMatricula_aluno() {
		return matricula_aluno;
	}
	public void setMatricula_aluno(Aluno matricula_aluno) {
		this.matricula_aluno = matricula_aluno;
	}
	
	
}
