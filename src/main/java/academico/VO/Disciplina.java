package academico.VO;

public class Disciplina {
	private int cod_disciplina;
	private String nome_disciplina;
	private Professor matricula_prof;
	
	public int getCod_disciplina() {
		return cod_disciplina;
	}
	
	public void setCod_disciplina(int cod_disciplina) {
		this.cod_disciplina = cod_disciplina;
	}
	
	public String getNome_disciplina() {
		return nome_disciplina;
	}
	
	public void setNome_disciplina(String nome_disciplina) {
		this.nome_disciplina = nome_disciplina;
	}
	
	public int getMatricula_prof() {
		return matricula_prof.getMatricula_prof();
	}
	
	public void setMatricula_prof(int matricula_prof) {
		this.matricula_prof.setMatricula_prof(matricula_prof);
	}
	
}
