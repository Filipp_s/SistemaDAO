package academico.VO;

public class Professor {
	private int matricula_prof;
	private String disciplina;
	private String nome_prof;
	private Curso cod_curso;
	
	public int getMatricula_prof() {
		return matricula_prof;
	}
	
	public void setMatricula_prof(int matricula_prof) {
		this.matricula_prof = matricula_prof;
	}
	
	public String getDisciplina() {
		return disciplina;
	}
	
	public void setDisciplina(String disciplina) {
		this.disciplina = disciplina;
	}
	
	public String getNome_prof() {
		return nome_prof;
	}
	
	public void setNome_prof(String nome_prof) {
		this.nome_prof = nome_prof;
	}
	
	public int getCod_curso() {
		return cod_curso.getCod_curso();
	}
	
	public void setCod_curso(int cod_curso) {
		this.cod_curso.setCod_curso(cod_curso);
	}
	
	
}
