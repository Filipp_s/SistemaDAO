package academico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import academico.DAO.CursoDAOimpl;
import academico.VO.Aluno;
import conexao.Conexao;

public class Principal {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner lerUser = new Scanner(System.in);
		
		System.out.println("============ SISTEMA ACADEMICO_SEMINARIO ==========");
		System.out.println("============ ESCOLHA SEU TIPO DE USUARIO ==========");
		System.out.println("1 - ADM");
		System.out.println("2 - ALUNO");
		System.out.println("3- SAIR");
		
		int opUser = lerUser.nextInt();
		switch (opUser) {
		case 1:
			System.out.println("============ ADMINISTRADOR ==========");
			System.out.println("1 - CADASTRAR PROFESSOR");
			System.out.println("2 - CADASTRAR ALUNO");
			System.out.println("3 - LISTAR ALUNOS");
			System.out.println("4 - LISTAR CURSOS");
			int opCad = lerUser.nextInt();
			switch (opCad) {
			case 1:
				System.out.println("============ CADASTRAR PROFESSOR ============ ");
				
				break;
			case 2:
				System.out.println("============ CADASTRAR ALUNO ============ ");
				//AlunoDAOImpl aluno = new AlunoDAOImpl(Aluno a);
					//aluno.cadastra(a);
				break;
			case 3:
				System.out.println("============ LISTAR ALUNOS ============ ");
				Connection conec = Conexao.conectaBanco();
				PreparedStatement pdst = null;
				ResultSet rsA = null;
				
				List<Aluno> alunos = new ArrayList<>();
				
				try {
				 pdst = conec.prepareStatement("SELECT * FROM Aluno ORDER BY matricula_aluno");
				 rsA = pdst.executeQuery();
				 
				 while (rsA.next()) {
					 Aluno a = new Aluno();
					 
					 a.setMatricula_aluno(rsA.getInt("matricula_aluno"));
					 a.setNome_aluno(rsA.getString("nome_aluno"));
					 a.setTurno(rsA.getString("turno"));
					 //a.setCod_curso(rsA.getInt("cod_curso"));
					 
					 	int matricula_aluno	 = rsA.getInt(1);
						String nome_aluno = rsA.getString(2);
						String turno = rsA.getString(3);
						//int codCurso = rsA.getInt(4);
						System.out.println("matricula_aluno: " + matricula_aluno);
						System.out.println("nome_aluno: " + nome_aluno);
						System.out.println("turno: " + turno);
						//System.out.println("codCurso: " + codCurso);
						System.out.println("");
					 
					 alunos.add(a);
					 			
				 }
				}catch(SQLException e) {
					System.out.println(e );
				}
				break;
			case 4:
				System.out.println("============ LISTA DE CUSOS ==========");
				CursoDAOimpl cursos = new CursoDAOimpl();
				cursos.listaCursos();
				break;
			}
			break;
		case 2:
			System.out.println("============ ALUNO ==========");
			@SuppressWarnings("resource")
			Scanner ler1 = new Scanner(System.in);
			
			System.out.println("============ ESCOLHA UMA OP��O ==========");
			System.out.println("1 - LISTAR CURSOS");
			System.out.println("2 - VIZUALIZAR BOLETIM");
			
			int op = ler1.nextInt();
			switch (op) {
			case 1:
				CursoDAOimpl cursos = new CursoDAOimpl();
				cursos.listaCursos();
				break;
			case 2:
					Connection con = Conexao.conectaBanco();
					try {
						 @SuppressWarnings("resource")
							Scanner lido = new Scanner(System.in);
							 
						 System.out.println("Digite um ID: ");
						 int IdAluno = lido.nextInt();
						 String query=
								 "select a.matricula_aluno, a.nome_aluno, d.nome_disciplina, valor_nota from sis_academico.nota n " + 
								 "inner join sis_academico.aluno a on a.matricula_aluno = n.matricula_aluno " + 
								 "inner join sis_academico.disciplina d on d.cod_disciplina = n.cod_disciplina " + 
								 "where a.matricula_aluno = ?";
						 PreparedStatement p = con.prepareStatement(query);
						 p.setInt(1, IdAluno);
						 ResultSet rs= p.executeQuery();

						 while (rs.next()){
								
								int matricula_aluno	 = rs.getInt(1);
								String nome_aluno = rs.getString(2);
								String nome_disciplina = rs.getString(3);
								int nota = rs.getInt(4);
								System.out.println("===================================================================");
								System.out.println("| IdAluno |     nome_aluno    |         Disciplina        |  nota |");
								System.out.println("-------------------------------------------------------------------");
								System.out.print("|     " +matricula_aluno);
								System.out.print("   |    " + nome_aluno);
								System.out.print("    |    " + nome_disciplina);
								System.out.println("    |  " + nota + "  |");
								System.out.println("-------------------------------------------------------------------");
								System.out.println("");
								}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
							
				
				break;	
			
			}
			break;
		}	
			
		
	
			
	
	}	
}			



 